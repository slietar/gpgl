import gpgl from '../../lib/index.js';


const gravity = 3e-6;
const nObjects = 20;

let kernel = gpgl({
  inputs: [
    { name: 'dt' },
    { name: 'time' },
    { name: 'gravity' },
    { name: 'objKinematics', size: [nObjects], length: 4 },
    { name: 'objMasses', size: [nObjects], length: 1 }
  ],
  output: {
    length: 4,
    size: [nObjects]
  },

  shader: `
    vec4 kernel(float pos, float size) {
      vec4 kinematics = objKinematics(pos);
      float mass = objMasses(pos);

      vec2 position = kinematics.xy;
      vec2 velocity = kinematics.zw;

      position += velocity * dt;

      for (int i = 0; i < ${nObjects}; i++) {
        if (i == int(pos)) {
          continue;
        }

        vec2 otherPosition = objKinematics(float(i)).xy;
        float otherMass = objMasses(float(i));

        vec2 delta = otherPosition - position;
        velocity += gravity * otherMass * normalize(delta) / pow(max(length(delta), 0.01), 2.0) * dt;
      }

      return vec4(position, velocity);
    }
  `,

  fallback: (ctx) => (index, size) => {
    let [posX, posY, velX, velY] = ctx.objKinematics(index);
    let mass = ctx.objMasses(index);

    posX += velX * ctx.dt;
    posY += velY * ctx.dt;

    for (let i = 0; i < size; i++) {
      if (i === index) {
        continue;
      }

      let [otherX, otherY] = ctx.objKinematics(i);
      let otherMass = ctx.objMasses(i);

      let deltaLength = Math.sqrt((otherX - posX) ** 2 + (otherY - posY) ** 2);

      velX += ctx.gravity * otherMass * (otherX - posX) / deltaLength / (Math.max(deltaLength, 0.01) ** 2) * ctx.dt;
      velY += ctx.gravity * otherMass * (otherY - posY) / deltaLength / (Math.max(deltaLength, 0.01) ** 2) * ctx.dt;
    }

    return [posX, posY, velX, velY];
  }
});


let objMasses = new Array(nObjects).fill(0).map((_, index) => {
  if (index === 0) {
    return 1000;
  }

  return Math.random() * 10;
}).map((m) => [m, 0, 0, 0]).flat();


let objKinematics = new Array(nObjects).fill(0).map((_, index) => {
  if (index === 0) {
    return [0.5, 0.5, 0, 0];
  }

  let angle = Math.random() * Math.PI * 2;
  let radius = 0.1 + Math.random() * 0.2;
  let velocity = Math.sqrt(gravity * objMasses[0] / radius);

  return [
    0.5 + Math.cos(angle) * radius,
    0.5 + Math.sin(angle) * radius,
    Math.sin(angle) * velocity,
    -Math.cos(angle) * velocity
  ];
}).flat();


let stats = new Stats();
document.body.appendChild(stats.dom);


let canvas = document.createElement('canvas');
let ctx = canvas.getContext('2d');

canvas.width = Math.min(window.innerWidth, window.innerHeight);
canvas.height = canvas.width;

document.body.appendChild(canvas);


let draw = () => {
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  for (let index = 0; index < objKinematics.length; index += 4) {
    let x = objKinematics[index + 0] * canvas.width;
    let y = objKinematics[index + 1] * canvas.height;
    let mass = objMasses[index];

    ctx.beginPath();
    ctx.arc(x, y, Math.pow(mass, 1/5) * 2, 0, Math.PI * 2);

    ctx.fillStyle = index === 0 ? 'red' : 'black';
    ctx.fill();
  }

  requestAnimationFrame(draw);
};

requestAnimationFrame(draw);


let time = Date.now();

setInterval(() => {
  let dt = Date.now() - time;
  time += dt;

  stats.begin();

  objKinematics = kernel({
    dt: dt * 1e-3,
    time,
    gravity,
    objKinematics,
    objMasses
  });

  stats.end();
}, 1000 / 60);

