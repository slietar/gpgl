import gpgl from '../../lib/index.js';


let canvas1 = document.createElement('canvas');
let ctx1 = canvas1.getContext('2d');
document.body.appendChild(canvas1);

canvas1.width = 300;
canvas1.height = 300;

ctx1.beginPath();
ctx1.arc(200, 50, 15, 0, Math.PI * 2);
ctx1.fill();


let canvas2 = document.createElement('canvas');
document.body.appendChild(canvas2);

canvas2.width = canvas1.width;
canvas2.height = canvas1.width;


let renderCanvas = gpgl({
  inputs: [{ name: 'source', length: 4, size: [canvas1.width, canvas1.height] }],
  output: canvas2,

  shader: `
    vec4 kernel(vec2 thread, vec2 resolution) {
      float dist = distance(thread, vec2(200, 50));
      return (vec4(1, 0, 0, 1)) * (source(thread).a + 10.0 / dist);
    }
  `
});


let stats = new Stats();
document.body.appendChild(stats.dom);


let render = () => {
  stats.begin();
  renderCanvas({
    source: canvas1
  });
  stats.end();
};

requestAnimationFrame(render);

