# GPGL


## Usage

```js
const gpgl = require('gpgl');

let kernel = gpgl({
  input: [
    { name: 'factor' }
  ],
  output: {
    size: [150, 60],
    length: 3
  },

  shader: `
    vec3 kernel(vec2 thread, vec2 size) {
      return vec3(thread * factor, thread.x + thread.y);
    }
  `,

  fallback: (context) => (threadX, threadY, sizeX, sizeY) => {
    return [
      threadX * context.factor,
      threadY * context.factor,
      threadX + threadY
    ];
  }
});

let result = kernel(6);
```



## Options


### Inputs & outputs

#### Arrays

Defining arrays is straightforward.

```js
{ name: 'arr', size: 5 }
{ name: 'arr', size: [5] }
```

As always, more components can be added with `length`. In GLSL, access to the values can be achieved by calling a function with the same name as the parameter. The size of the array can be obtained by adding `_size` to its name.

```glsl
float kernel(float position, float size) {
  return arr(position) / arr_size;
}
```

#### Matrices

```js
// 4x2 matrix with 1 component
{ size: [2, 4] }

// 4x2 matrix with 3 components
{ size: [2, 4], length: 3 }
```

```glsl
sum += a(3, 4);
```


### (Un) flattening [WIP]

A few options are available to manipulate the structure of the input or output values. Although they can be handy, they can add a significant delay if the data is too large.

- **`groupComponents`** – If true, components will not be flattened but appear as `[x, y]`, `[x, y, z]` or `[x, y, z, w]` depending on the length. Not applicable when the length is 1.
- **`groupDimensions`** – If true, each row will be contained in its own array. For instance, a 2x2 matrix would look like `[[1, 2], [3, 4]]` instead of `[1, 2, 3, 4]`. Not applicable when the size is one-dimensional.


### Fallback

Setting `useFallback` to true will force the use of the fallback kernel.

