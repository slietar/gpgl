export default function (options) {
  let renderMode = options.output instanceof HTMLCanvasElement;

  let outputParam = renderMode
    ? getParameter({ length: 4, size: [options.output.width, options.output.height] })
    : getParameter(options.output);
  let [width, height] = outputParam.size;

  let inputs = options.inputs || [];
  let inputParams = inputs.map((input) => getParameter(input));


  let canvas;

  if (renderMode) {
    canvas = options.output;
  } else {
    canvas = document.createElement('canvas');

    canvas.width = width;
    canvas.height = height;
  }

  let gl = canvas.getContext('webgl');

  if (!gl || options.useFallback) {
    return fallback(options);
  }

  gl.getExtension('OES_texture_float');
  gl.getExtension('WEBGL_color_buffer_float');


  // Shader initialization

  let vertexShaderSource = `precision lowp float;
precision lowp int;
precision lowp sampler2D;

attribute vec2 a_position;
uniform vec2 _resolution;
varying vec2 _position;

void main() {
  gl_Position = vec4(a_position.xy / _resolution.xy * 2.0 - 1.0, 0, 1);
  _position = ${renderMode ? 'vec2(a_position.x, _resolution.y - a_position.y)' : 'a_position.xy'};
}`;

let getAccessMember = (length) => ['.x', '.xy', '.xyz', ''][length - 1];
let getType = (length) => length > 1 ? `vec${length}` : 'float';

let fragmentShaderSource = `precision lowp float;
precision lowp int;
precision lowp sampler2D;

uniform vec2 _resolution;
varying vec2 _position;

${inputParams.map((param) => `uniform ${param.dimensions > 0 ? 'sampler2D' : getType(param.length)} ${param.dimensions > 0 ? '_p_' : ''}${param.name};`).join('\n')}

${
  inputParams
    .filter((param) => param.dimensions > 0)
    .map((param) =>
      `${getType(param.length)} ${param.name}(${getType(param.dimensions)} pos) {\n  return texture2D(_p_${param.name}, ${param.dimensions === 2 ? `pos / vec2(${param.size[0]}, ${param.size[1]})` : `vec2(pos / ${param.size[0]}.0, 0)`})${getAccessMember(param.length)};\n}`
    )
    .join('\n\n')
}

${options.shader}

void main() {
  gl_FragColor = vec4(kernel(_position${outputParam.dimensions === 1 ? '.x, _resolution.x' : ', _resolution'})${', 0'.repeat(4 - outputParam.length)});
}`;

  // console.info(fragmentShaderSource.split('\n').map((line, index) => `${index + 1} | ${line}`).join('\n'));

  let vert = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  let frag = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  let program = createProgram(gl, vert, frag);


  // Attributes/uniforms localization

  let positionAttributeLocation = gl.getAttribLocation(program, 'a_position');
  let resolutionUniformLocation = gl.getUniformLocation(program, '_resolution');

  let inputUniformLocations = inputParams.map((param) =>
    gl.getUniformLocation(program, (param.dimensions > 0 ? '_p_' : '') + param.name)
  );

  let inputTextures = new Map(inputParams
      .map((param, index) => [index, param])
      .filter(([index, param]) => param.dimensions > 0)
      .map(([index, param]) => [index, createTexture(gl)]));


  // Triangles definition

  let positionBuffer = gl.createBuffer();

  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  let positions = [
    0, 0,
    width, 0,
    width, height,

    0, 0,
    width, height,
    0, height
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);


  // Texture preparation

  let texture = null;

  if (!renderMode) {
    let data = new Float32Array(width * height * 4);
    texture = createTexture(gl, width, height, data);
  }


  return (...fnArgs) => {
    let args = getArguments(fnArgs, inputs);

    gl.viewport(0, 0, width, height);
    gl.clearColor(0, 0, 0, 0);
    gl.clear(gl.COLOR_BUFFER_BIT);

    if (!renderMode) {
      gl.bindFramebuffer(gl.FRAMEBUFFER, gl.createFramebuffer());
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);

      let frameBufferStatus = (gl.checkFramebufferStatus(gl.FRAMEBUFFER) == gl.FRAMEBUFFER_COMPLETE);
      if (!frameBufferStatus) {
        throw new Error('Invalid frame buffer');
      }
    }

    gl.useProgram(program);

    gl.enableVertexAttribArray(positionAttributeLocation);
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.vertexAttribPointer(positionAttributeLocation, 2, gl.FLOAT, false, 0, 0);

    gl.uniform2f(resolutionUniformLocation, gl.canvas.width, gl.canvas.height);

    for (let index = 0; index < inputParams.length; index++) {
      let param = inputParams[index];
      let name = inputs[index].name;
      let value = args[index];

      if (param.dimensions > 0) {
        let texture = inputTextures.get(index);

        gl.activeTexture(gl.TEXTURE0 + index);
        gl.bindTexture(gl.TEXTURE_2D, texture);

        if (value instanceof HTMLCanvasElement) {
          gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, value);
        } else {
          gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, param.size[0], param.size[1], 0, gl.RGBA, gl.FLOAT, new Float32Array(value));
        }

        gl.uniform1i(inputUniformLocations[index], index);
      } else {
        gl[`uniform${param.length}f`](inputUniformLocations[index], ...(param.length > 1 ? value : [value]));
      }
    }

    gl.drawArrays(gl.TRIANGLES, 0, positions.length / 2);

    if (!renderMode) {
      let pixels = new Float32Array(gl.drawingBufferWidth * gl.drawingBufferHeight * 4);
      gl.readPixels(0, 0, width, height, gl.RGBA, gl.FLOAT, pixels);

      return pixels;
    }
  };
}



function createProgram(gl, vertexShader, fragmentShader) {
  let program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);

  let success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }

  console.error(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);

  throw new Error('Failed to create program');
}

function createShader(gl, type, source) {
  let shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);

  let success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.error(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);

  throw new Error('Failed to create shader');
}

function createTexture(gl, width, height, data) {
  let texture = gl.createTexture();

  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

  if (data) {
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.FLOAT, data);
  }

  return texture;
}

function fallback(options) {
  let inputs = options.inputs || [];
  let outputParam = getParameter(options.output);

  return (...fnArgs) => {
    let args = getArguments(fnArgs, inputs);
    let context = {};

    for (let index = 0; index < inputs.length; index++) {
      let input = inputs[index];
      let value = args[index];
      let param = getParameter(input);

      if (param.dimensions === 1) {
        context[input.name] = (x) => {
          let i = x * 4;
          return value.slice(i, i + param.length);
        };
      } else if (param.dimensions === 2) {
        context[input.name] = (x, y) => {
          let i = (y * input.size[0] + x) * 4;
          return value.slice(i, i + param.length);
        };
      } else {
        context[input.name] = args[index];
      }
    }

    let fn = options.fallback(context);
    let [width, height] = outputParam.size;
    let result = new Array(width * height * 4).fill(0);

    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        let pixel;

        if (outputParam.dimensions === 2) {
          pixel = fn(x, y, width, height);
        } else {
          pixel = fn(x, width);
        }

        result.splice((y * width + x) * 4, outputParam.length, ...pixel);
      }
    }

    return result;
  };
}

function getArguments(args, inputs) {
  return args.length === 1 && typeof args[0] === 'object' && !Array.isArray(args[0])
    ? inputs.map((input) => args[0][input.name])
    : args;
}

function getParameter(param) {
  return {
    dimensions: param.size ? (param.size.length || 1) : 0,
    length: param.length || 1,
    name: param.name || null,
    size: param.size ? [
      param.size[0] || param.size || 1,
      param.size[1] || 1
    ] : null
  };
}

